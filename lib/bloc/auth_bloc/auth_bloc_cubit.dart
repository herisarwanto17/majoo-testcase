
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:majootestcase/db/database_provider.dart';
import 'package:majootestcase/db/model/user_model.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit({DatabaseProvider? databaseProvider})
      : _databaseProvider = databaseProvider,
        super(AuthBlocInitialState());

  final DatabaseProvider? _databaseProvider;

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser(User user) async {
    var userModel = UserModel(id: null, email: user.email, username: user.userName, password: user.password);
    emit(AuthBlocLoadingState());
    await Future.delayed(const Duration(seconds: 2));
    try {
      final login = await _databaseProvider!.login(userModel);

        if (login.isNotEmpty) {
          SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
          await sharedPreferences.setBool("is_logged_in", true);
          String data = user.toJson().toString();
          sharedPreferences.setString("user_value", data);
          emit(AuthBlocLoggedInState());
        } else {
          emit(AuthBlocSuccesState());
        }
    } catch (e) {
      debugPrint("$runtimeType, login_user CATCH ==> ${e.toString()} ");
      // emit(AuthBlocErrorState(e));
      AuthBlocSuccesState();
    }
  }

  Future<void> saveUser(String email, String username, String password) async {
    emit(AuthBlocLoadingState());
    UserModel editUser = UserModel(id: null, email: email, username: username, password: password);
    await Future.delayed(const Duration(seconds: 2));
    try {
      editUser = await _databaseProvider!.save(editUser);
      emit(AuthBlocSuccesState());
    } catch (e) {
      debugPrint("$runtimeType, saveUser CATCH ==> ${e.toString()} ");
      emit(AuthBlocErrorState(e));
    }
  }
}






















