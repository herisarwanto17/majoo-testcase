import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/models/movie_response.dart';

import 'package:majootestcase/services/api_service.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/user.dart';
import '../../ui/auth/login_page.dart';
import '../auth_bloc/auth_bloc_cubit.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());
  ApiServices apiServices = ApiServices();
  int page = 1;

   Future fetchingData() async {
     emit(HomeBlocInitialState());
     try {
       final result = await InternetAddress.lookup('google.com');
       if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
         MovieResponse? movieResponse = await apiServices.getMovieList(1);
         if (movieResponse == null) {
           emit(HomeBlocErrorState(
               "an error occured, please try again later"));
         } else {
           emit(HomeBlocLoadedState(movieResponse.data));
         }
       }
     } on SocketException catch (_) {
       emit(HomeBlocErrorState("no internet connection"));
     }
  }

  void handleClick(String value, BuildContext context) {
    switch (value) {
      case 'Logout':
        signOut(context);
        break;
    }
  }

  Future loadMore(BuildContext context) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        loading(context);
        MovieResponse? movieResponse = await apiServices.getMovieList(page);
        Navigator.pop(context);
        if (movieResponse == null) {
          emit(HomeBlocErrorState(
              "an error occured, please try again later"));
        } else {
          emit(HomeBlocLoadedState(movieResponse.data));
        }
      }
    } on SocketException catch (_) {
      emit(HomeBlocErrorState("no internet connection"));
    }
  }

  loading(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return const AlertDialog(
          backgroundColor: Colors.transparent,
          content: Center(
            child: CircularProgressIndicator(
              color: kMikadoYellow,
              strokeWidth: 8,
            ),
          ),
        );
      },
    );
  }

  void signOut(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: const [
              Text(
                'Informasi',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Apakah anda yakin ingin logout?',
                    maxLines: 4,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              child: const Text(
                'Tidak',
                style: TextStyle(
                  color: kMikadoYellow,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text(
                'Ya',
                style: TextStyle(
                  color: kMikadoYellow,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                await prefs.remove("is_logged_in");
                // prefs.clear();
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (_) => BlocProvider(
                      create: (context) => AuthBlocCubit()..fetchHistoryLogin(),
                      child: LoginPage(),
                    ),
                  ),
                      (Route<dynamic> route) => false,
                );
              },
            ),
          ],
        );
      },
    );
  }
}



























