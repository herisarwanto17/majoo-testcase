import 'package:flutter/material.dart';

class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const API_KEY = "59bdbc058bf8a49a02af0a7afb1b5326";
  static const BASE_URL = "https://api.themoviedb.org/3/trending/all/day?api_key=$API_KEY";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
  static const TRENDING = "/trending/all/day?api_key=0bc9e6490f0a9aa230bd01e268411e10";

}

class Font {
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}

const Color kRichBlack = Color(0xFF151C26);
const Color kMikadoYellow = Color(0xFFF4C10F);
const Color titleColor = Color(0xFF5A606B);
