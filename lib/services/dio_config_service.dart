import 'dart:async';
import 'package:dio/dio.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

late Dio dioInstance;
createInstance(int page) async {

  var option = BaseOptions(
    baseUrl: Api.BASE_URL+"&page=$page",
    connectTimeout: 10000,
    receiveTimeout: 10000
  );


  dioInstance = new Dio(option);
  dioInstance.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));

}

Future<Dio> dio(int page) async {
  await createInstance(page);
  dioInstance.options.baseUrl = Api.BASE_URL+"&page=$page";
  return dioInstance;
}

