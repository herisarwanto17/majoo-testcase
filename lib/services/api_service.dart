
import 'dart:convert';

import 'package:dio/dio.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

import '../models/movie_response.dart';

class ApiServices{

  Future<MovieResponse?> getMovieList(page) async {
    try {
      var dio = await (dioConfig.dio(page));
      Response<String> response = await dio.get("");
      MovieResponse movieResponse =
      MovieResponse.fromJson(jsonDecode(response.data!));
      return movieResponse;
    } catch (error) {
      return null;
    }
  }
}

















