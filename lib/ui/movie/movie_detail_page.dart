import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:majootestcase/models/movie_response.dart';


class MovieDetailPage extends StatelessWidget {
  const MovieDetailPage({Key? key, required this.data}) : super(key: key);
  final Data data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: [
          ScrollConfiguration(
            behavior: const ScrollBehavior().copyWith(overscroll: false),
            child: ListView(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              children: [
                CachedNetworkImage(
                  imageUrl:
                      'https://image.tmdb.org/t/p/w500/${data.posterPath}',
                  placeholder: (context, url) =>
                      const Center(child: CircularProgressIndicator()),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                  fit: BoxFit.cover,
                  width: double.infinity,
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            padding: const EdgeInsets.fromLTRB(5, 2, 5, 3),
                            decoration: BoxDecoration(
                              color: Colors.white12,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Row(
                              children: [
                                Text(
                                  'Rating : ',
                                  style: const TextStyle(color: Colors.white),
                                ),
                                const Icon(
                                  Icons.star,
                                  color: Colors.yellow,
                                  size: 15,
                                ),
                                const SizedBox(width: 4),
                                Text(
                                  '${data.voteAverage}',
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(width: 10),
                          Container(
                            padding: const EdgeInsets.fromLTRB(8, 2.5, 8, 2.5),
                            decoration: BoxDecoration(
                              color: Colors.white12,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Text(
                              data.mediaType == null
                                  ? '-'
                                  : data.mediaType!.toUpperCase(),
                              style: const TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 10),
                      Text(
                        data.title ?? '-',
                        style: const TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(height: 5),
                      Text(
                        data.overview ?? '-',
                        style: const TextStyle(color: Colors.white),
                      ),
                      const SizedBox(height: 20),
                      Text(
                        'Release Date :  ${data.releaseDate == '-' ? '-' : DateFormat("dd MMMM yyyy").format(DateTime.parse(data.releaseDate!))}',
                        style: const TextStyle(color: Colors.white),
                      ),
                      SizedBox(height: 5,),
                      Text(
                        'Vote Count : ${data.voteCount}',
                        style: const TextStyle(color: Colors.white),
                      ),
                      SizedBox(height: 5,),
                      Text(
                        'Popularity : ${data.popularity}',
                        style: const TextStyle(color: Colors.white),
                      ),
                      const SizedBox(height: 25),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SafeArea(
            child: IconButton(
              onPressed: () => Navigator.pop(context),
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
