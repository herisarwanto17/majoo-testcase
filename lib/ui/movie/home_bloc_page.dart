
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/ui/movie/movie_list.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import '../extra/loading.dart';
import '../extra/error_screen.dart';

class HomeBlocPage extends StatelessWidget {
  const HomeBlocPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<HomeBlocCubit, HomeBlocState>(builder: (context, state) {
        if (state is HomeBlocLoadedState) {
          return MovieList(movie: state.data);
        }
        else if (state is HomeBlocLoadingState) {
          return LoadingIndicator();
        }
        else if (state is HomeBlocInitialState) {
          return LoadingIndicator();
        } else if (state is HomeBlocErrorState) {
          bool isNetworkError = '${state.error}'.contains('no internet');
          return ErrorScreen(
            message: state.error,
            icon: !isNetworkError ? null : Icons.wifi_off,
            retryButton: !isNetworkError
                ? const SizedBox()
                : ElevatedButton(
              onPressed: () =>
                  context.read<HomeBlocCubit>().fetchingData(),
              style: ElevatedButton.styleFrom(
                primary: Colors.red,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
              ),
              child: const Text(
                "REFRESH",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          );
        }

        return Center(
          child: Text(kDebugMode ? "state not implemented $state" : ""),
        );
      }),
    );
  }

}
