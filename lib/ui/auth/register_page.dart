import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/auth/login_page.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/data_extension.dart';

import '../../bloc/auth_bloc/auth_bloc_cubit.dart';
import '../../common/widget/custom_button.dart';
import '../../common/widget/text_form_field.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _emailController = TextController(initialValue: '');
  final _passwordController = TextController(initialValue: '');
  final _usernameController = TextController(initialValue: '');
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kRichBlack,
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is AuthBlocSuccesState) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Register Berhasil."),
            ));
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => LoginPage(),
              ),
            );
          } else if (state is AuthBlocErrorState) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Register Gagal!"),
            ));
            Navigator.of(context).pop();
          } else if (state is AuthBlocLoadingState) {
            LoadingIndicator();
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                Text(
                  'Silahkan register terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    color: Colors.white
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                SizedBox(
                  width: double.infinity,
                  child: CustomButton(
                    text: 'Kirim',
                    onPressed: handleRegister,
                    height: 100,
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                _register(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) => val!.isValidEmail() ? null : "email is invalid",
            suffixIcon: IconButton(
              icon: Icon(Icons.email),
              onPressed: () {},
            ),
          ),
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            isEmail: true,
            hint: 'Username',
            label: 'Username',
            // validator: (val) => val!.isValidEmail() ? null : "email is invalid",
            suffixIcon: IconButton(
              icon: Icon(Icons.person_outline),
              onPressed: () {},
            ),
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          Navigator.pop(context);
        },
        child: RichText(
          text: TextSpan(
              text: 'Sudah punya akun? ',
              style: TextStyle(color: Colors.white),
              children: [
                TextSpan(
                  text: 'Login',
                ),
              ]),
        ),
      ),
    );
  }

  void handleRegister() async {
    showAlertDialog();
    final _email = _emailController.value;
    final _password = _passwordController.value;
    final _username = _usernameController.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null &&
        _username != null) {
      context.read<AuthBlocCubit>().saveUser(_email, _username, _password);
    }
  }

  showAlertDialog() {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(margin: EdgeInsets.only(left: 5), child: Text("Loading")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
