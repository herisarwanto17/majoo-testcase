import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  const ErrorScreen({
    Key? key,
    this.gap = 10,
    this.retryButton,
    this.message = "",
    this.fontSize = 14,
    this.textColor,
    this.icon,
  }) : super(key: key);
  final String message;
  final Color? textColor;
  final double fontSize;
  final double gap;
  final Widget? retryButton;
  final IconData? icon;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            icon == null
                ? const SizedBox()
                : Icon(icon, size: 150, color: Colors.white),
            Text(
              message,
              style: TextStyle(fontSize: 15, color: textColor ?? Colors.white),
            ),
            retryButton != null
                ? Column(children: [const SizedBox(height: 10), retryButton!])
                : const SizedBox()
          ],
        ),
      ),
    );
  }
}
