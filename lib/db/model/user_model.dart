class UserModel {
  int? id;
  String? email;
  String? username;
  String? password;

  UserModel({this.id, this.email, this.username, this.password});

  UserModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        email = json['email'],
        password = json['password'],
        username = json['username'];

  Map<String, dynamic> toJson() =>
      {'id': id, 'email': email, 'password': password, 'username': username};

  UserModel copy({
    int? id,
    String? email,
    String? username,
    String? password,
  }) =>
      UserModel(
        id: id ?? this.id,
        email: email ?? this.email,
        username: username ?? this.username,
        password: password ?? this.password,
      );
}
