import 'package:majootestcase/db/model/user_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

const DB_NAME = "users.db";

class DatabaseProvider {
  static final DatabaseProvider instance = DatabaseProvider._init();
  static Database? _db;

  DatabaseProvider._init();

  Future<Database> get db async {
    if (_db != null) return _db!;
    _db = await _useDatabase(DB_NAME);

    return _db!;
  }

  Future<Database> _useDatabase(String filePath) async {
    final dbPath = await getDatabasesPath();

    return await openDatabase(
      join(dbPath, DB_NAME),
      onCreate: (db, version) {
        return db.execute(
            'CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT, username TEXT, password TEXT)');
      },
      version: 1,
    );
  }

  Future<List<UserModel>> getUser() async {
    final db = await instance.db;
    final result = await db.rawQuery('SELECT * FROM users ORDER BY id');
    // print(result);
    return result.map((json) => UserModel.fromJson(json)).toList();
  }

  Future<List<UserModel>> login(UserModel user) async {
    final db = await instance.db;
    final result = await db.rawQuery('SELECT * FROM users WHERE email = ? and password = ?',
        [user.email, user.password]);
    return result.map((json) => UserModel.fromJson(json)).toList();
  }

  Future<UserModel> save(UserModel user) async {
    final db = await instance.db;
    final id = await db.rawInsert(
        'INSERT INTO users (email, username, password) VALUES (?,?,?)',
        [user.email, user.username, user.password]);

    print('$runtimeType, user id $id success created.');
    return user.copy(id: id);
  }

  Future close() async {
    final db = await instance.db;
    db.close();
  }
}
























