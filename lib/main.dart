import 'package:majootestcase/db/database_provider.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/movie/home_bloc_page.dart';
import 'package:majootestcase/ui/auth/login_page.dart';
import 'package:flutter/foundation.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/home_bloc/home_bloc_cubit.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (BuildContext context) => DatabaseProvider.instance,
      child: BlocProvider(
        create: (context) =>
        AuthBlocCubit(databaseProvider: DatabaseProvider.instance)
          ..fetchHistoryLogin(),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: MyHomePageScreen(),
        ),
      ),
    );
  }
}

class MyHomePageScreen extends StatelessWidget {
  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<AuthBlocCubit, AuthBlocState>(
          builder: (context, state) {
            if (state is AuthBlocLoginState) {
              return BlocProvider(
                create: (context) => AuthBlocCubit(databaseProvider: DatabaseProvider.instance),
                child: LoginPage(),
              );
            }
            else if (state is AuthBlocLoggedInState) {
              return BlocProvider(
                create: (context) =>
                HomeBlocCubit(
                )
                  ..fetchingData(),
                child: HomeBlocPage(),
              );
            }
            return LoadingIndicator();
          }),
    );
  }
}

